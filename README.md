# README #

Free Game Art - Volume 1

### What is this repository for? ###

* This repo contains free game line art for use in demo's, prototypes, etc.

### Folders ###

There are three folders:

* arrow
* butterfly
* ghost

Each folder contains line art of a graphic in various colors and saved in PNG format.

The butterfly and ghost folders also contain some animated frames, labled *1.png, *2.png, etc. which can be used in texture animation.

### Pixlemator Files ###

Each folder also contains the original Pixelmator (*.pxm) file used to created each graphic.

Pixelmator is a drawning program available from the Mac App Store.

To modify and export a graphic, hide all other layers and export while only the layer you are interested in is visible.